import React from 'react';
import { render } from 'react-dom';
import App from './components/App';
import { createStore } from './rx/flux';

const initialState = { counter: 0 };

createStore(initialState).subscribe(state =>
  render(<App {...state} />, document.getElementById('app'))
);
