import React, { Fragment } from 'react';
import NavBar from './NavBar';
import ButtonGroup from './ButtonGroup';

const App = ({ counter }) => (
  <Fragment>
    <NavBar counter={counter} />
    <ButtonGroup />
  </Fragment>
);

export default App;
