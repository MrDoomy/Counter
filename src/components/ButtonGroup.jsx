import React from 'react';
import { object } from 'prop-types';
import { withStyles } from '@material-ui/core';
import { Button } from '@material-ui/core';
import { incrementCounter, decrementCounter } from '../rx/actions';

const handleIncrementCounter = range => () => incrementCounter(range);

const handleDecrementCounter = range => () => decrementCounter(range);

// Material UI
const styles = {
  buttonGroup: {
    marginTop: 20,
    textAlign: 'center'
  },
  button: {
    margin: '0px 10px'
  }
};

const ButtonGroup = ({ classes }) => (
  <div className={classes.buttonGroup}>
    <Button className={classes.button} variant="contained" onClick={handleIncrementCounter()}>Increment</Button>
    <Button className={classes.button} variant="contained" onClick={handleDecrementCounter()}>Decrement</Button>
  </div>
);

ButtonGroup.propTypes = {
  classes: object.isRequired
};

export default withStyles(styles)(ButtonGroup);