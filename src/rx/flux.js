import { Observable, Subject } from 'rxjs';

import 'rxjs/add/observable/from';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/scan';
import 'rxjs/add/operator/startWith';

import reducer from './reducer';

// Utils
const isObservable = obs => obs instanceof Observable;

const action$ = new Subject();

/**
 * Creating a store for state management 
 *
 * @param {Any} initialState Initial state of store
 * @returns {Observable} The store
 */
export const createStore = initialState => {
  return action$
    .flatMap(action => isObservable(action) ? action : Observable.from([action]))
    .startWith(initialState)
    .scan(reducer);
};

/**
 * Creating a state management action from a function
 *
 * @param {Function} func Function of store
 * @returns {Any} The action
 */
export const actionCreator = func => (...args) => {
  const action = func.call(null, ...args);

  action$.next(action);

  if (isObservable(action.range)) {
    action$.next(action.range);
  }

  return action;
};
